#pragma once

#include "common.h"

#define VM_MEMSZ (64<<10)
#define VM_MAXINPUT 128
#define VM_MAXOUTPUT 128
struct vm_ctx;

enum vm_state {
    VM_SRUNNING,
    VM_SHALTED,
    VM_SERROR,
};

enum vm_stopreason {
    VM_EHALTED,
    VM_EINPUT,
    VM_EOUTPUT,
};

typedef int (*vm_read_t)(struct vm_ctx *ctx, i64 x);
typedef int (*vm_write_t)(struct vm_ctx *ctx, i64 x);

struct vm_ctx {
    i64 *ip;
    i64 bp;

    int state;
    vm_read_t readcb;
    vm_write_t writecb;

    i64 qinput[VM_MAXINPUT];
    int inlen;

    i64 qoutput[VM_MAXOUTPUT];
    int outlen;

    i64 mem[VM_MEMSZ];
};

int vm_init(struct vm_ctx *ctx, const i64 *mem, size_t n, vm_read_t readcb, vm_write_t writecb);
int vm_push_input(struct vm_ctx *ctx, i64 in);
int vm_pop_output(struct vm_ctx *ctx, i64 *out);
int vm_run(struct vm_ctx *ctx);

static inline int vm_ishalted(struct vm_ctx *ctx) { return ctx->state == VM_SHALTED;}
