#include <unistd.h>

#include "common.h"
#include "intcode.h"

static i64 program[VM_MEMSZ];
struct vm_ctx vm;

#define W 50
#define H 30
char grid[H][W];

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    i64 x;

    do {
        if (fscanf(f, "%" SCNi64 , &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < VM_MEMSZ);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

static void
draw_grid(void)
{
    fputs("\033[2J", stdout);
    for (int i=0; i<H; i++) {
        for (int j=0; j<W; j++) {
            switch(grid[i][j]) {
            case 0: fputc(' ', stdout); break;
            case 1: fputc('#', stdout); break;
            case 2: fputc('@', stdout); break;
            case 3: fputc('=', stdout); break;
            case 4: fputc('*', stdout); break;
            }
        }
        fputc('\n', stdout);
    }
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    size_t n;
    read_program(program, &n, stdin);

    program[0] = 2;
    vm_init(&vm, program, n, NULL, NULL);

    int part1 = 0;
    i64 ballx = 0;
    i64 paddlex = 0;
    i64 score = 0;
    bool started = false;

    while (!vm_ishalted(&vm)) {
        int err = vm_run(&vm);
        int i = 0;
        for (; i+3 <= vm.outlen; i+=3) {
            i64 x, y, tileid;
            x = vm.qoutput[i];
            y = vm.qoutput[i+1];
            tileid = vm.qoutput[i+2];
            if (x == -1 && y == 0) { score = tileid; continue;}
            grid[y][x] = tileid;
            if (tileid == 3) paddlex = x;
            if (tileid == 4) ballx = x;
            if (tileid == 4 || tileid == 3) {
                draw_grid();
                printf("score: %" PRIi64 "\n", score);
                usleep(2000);
            }
        }
        memmove(vm.qoutput, vm.qoutput+i, sizeof(i64 [vm.outlen - i]));
        vm.outlen -= i;
        if (err == VM_EINPUT) {
            if (!started) {
                for (int y=0; y<H; y++) {
                    for (int x=0; x<W; x++) {
                        part1 += (grid[y][x] == 2);
                    }
                }
            }
            started = true;
            vm_push_input(&vm, SIGN(ballx - paddlex));
        }
    }

    printf("%d\n", part1);
    printf("%" PRIi64 "\n", score);
    return 0;
}
