#include "common.h"

#define N 4
Vec3i moon_pos[N];
Vec3i moon_vel[N];
Vec3i cpy[N];

static size_t
read_moons(char line[], size_t len)
{
    int i=0;
    int x=0, y=0, z=0;
    while (fgets(line, len, stdin)) {
        if (sscanf(line, " <x=%d, y=%d, z=%d>", &x, &y, &z) != 3) {
            assert(!"bad input");
        }
        moon_pos[i++] = VEC3I(x,y,z);
    }
    assert(i == N);
    return i;
}

static void
update_vel(size_t k)
{
    for (size_t i=0; i<N; i++) {
        for (size_t j=i+1; j<N; j++) {
            const i32 d = SIGN(moon_pos[j].v[k] - moon_pos[i].v[k]);
            moon_vel[i].v[k] += d;
            moon_vel[j].v[k] -= d;
        }
    }
}

static void
update_pos(size_t k)
{
    for (size_t i=0; i<N; i++) {
        moon_pos[i].v[k] += moon_vel[i].v[k];
    }
}

static i64
total_energy(void)
{
    i64 total = 0;
    for (size_t i=0; i<N; i++) {
        total += (i64)vec3i_taxinorm(moon_pos[i]) * vec3i_taxinorm(moon_vel[i]);
    }
    return total;
}

static inline i64
lcm3(i64 a, i64 b, i64 c)
{
    i64 g1 = gcd(a, b);
    i64 l1 = (a / g1) * b;
    i64 g2 = gcd(l1, c);
    return (l1 / g2) * c;
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    char line[256];
    read_moons(line, sizeof(line));
    memcpy(cpy, moon_pos, sizeof(moon_pos));

    const int nsteps = 1000;
    i64 part1 = 0;
    i64 part2 = 0;

    for (int i=0; i<nsteps; i++) {
        update_vel(0);
        update_vel(1);
        update_vel(2);

        update_pos(0);
        update_pos(1);
        update_pos(2);
    }

    part1 = total_energy();

    memcpy(moon_pos, cpy, sizeof(moon_pos));
    memset(moon_vel, 0, sizeof(moon_vel));
    i64 fac[3] = {0};

    for (int k=0; k<3; k++) {
        for (size_t j=0; j != N; fac[k]++) {
            update_vel(k);
            update_pos(k);
            for (j=0; j<N; j++) {
                if (moon_vel[j].v[k])
                    break;
                if (moon_pos[j].v[k] != cpy[j].v[k])
                    break;
            }
        }
    }

    part2 = lcm3(fac[0], fac[1], fac[2]);

    printf("%" PRIi64 "\n", part1);
    printf("%" PRIi64 "\n", part2);
    return 0;
}
