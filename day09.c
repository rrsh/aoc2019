#include "common.h"

#define N ((size_t)1E6)
static i64 program[N];

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    unsigned long long x;

    do {
        if (fscanf(f, "%lld", &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < N);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

static i64 *
run_program(i64 *mem, size_t n, i64 *in)
{
    i64 *ip = mem;
    i64 bp = 0;

    while (1) {
        assert((size_t)(ip - mem) < n);
        /* see day5-gen.py */
        switch (*ip) {
        /* begin gen code */
#include "day5_interp.gen"
        /* end gen code */
        case 3:
            mem[ip[1]] = *in++;
            ip = ip + 2;
            break;
        case 203:
            mem[bp + ip[1]] = *in++;
            ip = ip + 2;
            break;
        case 4:
            printf("%" PRIi64 "\n", mem[ip[1]]);
            ip = ip + 2;
            break;
        case 104:
            printf("%" PRIi64 "\n",ip[1]);
            ip = ip + 2;
            break;
        case 204:
            printf("%" PRIi64 "\n", mem[bp + ip[1]]);
            ip = ip + 2;
            break;
        case 99:
            goto halt;
        default:
            printf("[ip] = %d\n", (int)*ip);
            assert(!"bad instruction");
        }
    }

halt:
    return ip;
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    size_t n = 0;
    read_program(program, &n, stdin);

    i64 in[] = {1};
    run_program(program, n, in);

    in[0] = 2;
    run_program(program, n, in);
    return 0;
}
