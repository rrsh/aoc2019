#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t sbyte;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#define N ((size_t)1E5)
static i64 program[N];

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    unsigned long long x;

    do {
        if (fscanf(f, "%lld", &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < N);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

static i64 *
run_program(i64 *mem, size_t n, i64 *in)
{
    i64 *ip = mem;
    while (1) {
        /* see day5-gen.py */
        switch (*ip) {
        /* begin gen code */
        case 1:
            mem[ip[3]] = (mem[ip[1]] + mem[ip[2]]);
            ip = ip + 4;
            break;
        case 2:
            mem[ip[3]] = (mem[ip[1]] * mem[ip[2]]);
            ip = ip + 4;
            break;
        case 5:
            if (mem[ip[1]]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 6:
            if (!mem[ip[1]]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 7:
            mem[ip[3]] = (mem[ip[1]] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 8:
            mem[ip[3]] = (mem[ip[1]] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 101:
            mem[ip[3]] = (ip[1] + mem[ip[2]]);
            ip = ip + 4;
            break;
        case 102:
            mem[ip[3]] = (ip[1] * mem[ip[2]]);
            ip = ip + 4;
            break;
        case 105:
            if (ip[1]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 106:
            if (!ip[1]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 107:
            mem[ip[3]] = (ip[1] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 108:
            mem[ip[3]] = (ip[1] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 1001:
            mem[ip[3]] = (mem[ip[1]] + ip[2]);
            ip = ip + 4;
            break;
        case 1002:
            mem[ip[3]] = (mem[ip[1]] * ip[2]);
            ip = ip + 4;
            break;
        case 1005:
            if (mem[ip[1]]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1006:
            if (!mem[ip[1]]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1007:
            mem[ip[3]] = (mem[ip[1]] < ip[2]);
            ip = ip + 4;
            break;
        case 1008:
            mem[ip[3]] = (mem[ip[1]] == ip[2]);
            ip = ip + 4;
            break;
        case 1101:
            mem[ip[3]] = (ip[1] + ip[2]);
            ip = ip + 4;
            break;
        case 1102:
            mem[ip[3]] = (ip[1] * ip[2]);
            ip = ip + 4;
            break;
        case 1105:
            if (ip[1]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1106:
            if (!ip[1]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1107:
            mem[ip[3]] = (ip[1] < ip[2]);
            ip = ip + 4;
            break;
        case 1108:
            mem[ip[3]] = (ip[1] == ip[2]);
            ip = ip + 4;
            break;
        /* end gen code */
        case 3:
            mem[ip[1]] = *in++;
            ip = ip + 2;
            break;
        case 104:
            printf("%" PRIi64 "\n",ip[1]);
            ip = ip + 2;
            break;
        case 4:
            printf("%" PRIi64 "\n", mem[ip[1]]);
            ip = ip + 2;
            break;
        case 99:
            goto halt;
        default:
            __builtin_unreachable();
        }
    }
halt:
    return ip;
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    size_t n = 0;

    read_program(program, &n, stdin);

    i64 in[] = {1234567890};
    run_program(program, n, in);

    return 0;
}
