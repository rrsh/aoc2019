#include <ctype.h>
#include "common.h"

#define ALPHSZ 36
#define N 2048

static int htab[ALPHSZ * ALPHSZ * ALPHSZ];
static int p[N];
static int path[N];
static size_t nnodes;
static int l[N];

static unsigned
hash(const char *str)
{
    unsigned value = 0;
    for (; *str; str++) {
        assert(isalnum(*str));
        unsigned x = isdigit(*str)? *str - '0' : (toupper(*str) - 'A') + 10;
        value = ALPHSZ * value + x;
    }
    return value;
}

static unsigned
node(const char *str)
{
    unsigned i = hash(str);
    return htab[i]? htab[i] : (htab[i] = nnodes++);
}

static unsigned
fill_path(size_t i)
{
    if (!i) { return 0;}
    return path[i]? path[i] : (path[i] = (1 + fill_path(p[i])));
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        if (!freopen(argv[1], "r", stdin)) {
            assert(!"freopen() failed");
        }
    }

    p[node("COM")] = 0;

    char a[4], b[4];
    while (scanf(" %3[A-Z0-9]) %3[A-Z0-9]", a, b) == 2) {
        p[node(b)] = node(a);
    }
    if (!feof(stdin)) {
        assert(!"bad input");
    }

    unsigned part1 = 0;
    unsigned part2 = 0;

    for (size_t i=1; i<nnodes; i++) {
        part1 += fill_path(i) - 1;
    }

    size_t i;
    unsigned len = 0;

    for (i = node("SAN"); i; i = p[i]) {
        l[i] = len++;
    }
    for (i = node("YOU"); i; i = p[i]) {
        if (l[i]) {
            part2 += l[i];
            break;
        }
        part2++;
    }

    part2 -= 2;
    printf("%u\n", part1);
    printf("%u\n", part2);
    return 0;
}
