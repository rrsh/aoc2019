#include "common.h"

#define HTABSZ (16<<10)
static size_t htab[HTABSZ];
static size_t nvar;

#define N 60
static i64 eqtab[N][N];
static i64 producer[N];
static i64 sol[N];
static i64 tmp[N];
static size_t neq;

static u32
hash(const char *word)
{
    u32 hash = 0;
    while (*word)
        hash = 26*hash + (*word++ - 'A');
    return hash;
}

static inline size_t
makevar(const char *word)
{
    u32 i = hash(word) % HTABSZ;
    return htab[i]? htab[i] : (htab[i] = ++nvar);
}

static void
makeeq(char *line)
{
    int n = 0, qty;
    char word[17];

    do {
        if (sscanf(line, "%d %16[A-Z]%n", &qty, word, &n) != 2)
            assert(!"bad input");
        eqtab[neq][makevar(word)] += -qty, line += n;
    } while (*line++ == ',');

    if (sscanf(line, " =>%d %16[A-Z]", &qty, word) != 2)
        assert(!"bad input");

    const size_t j= makevar(word);
    eqtab[neq][j] += qty, producer[j] = neq++;
}

static void
row_add(i64 scale, i64 * restrict dest, const i64 * restrict src)
{
    for (size_t k=1; k<=nvar; k++)
        dest[k] = dest[k] + scale * src[k];
}

static void
satisfy_equation(i64 *eq)
{
    for (size_t i=0; i != nvar;) {
        for (i=2; i<=nvar; i++) {
            if (eq[i] < 0) {
                size_t j = producer[i];
                row_add(CEILDIV(-eq[i], eqtab[j][i]), eq, eqtab[j]);
                break;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    const u32 ore = makevar("ORE");
    const u32 fuel = makevar("FUEL");

    char line[1024];
    while (fgets(line, sizeof(line), stdin)) {
        makeeq(line);
    }

    const i64 funds = 1000000000000LLU;
    sol[ore] = funds;
    i64 part1 = 0;

    for (i64 scale=1; scale;) {
        memcpy(tmp, sol, sizeof(sol));
        row_add(scale, sol, eqtab[producer[fuel]]);

        satisfy_equation(sol);

        if (sol[ore] < 0) {
            memcpy(sol, tmp, sizeof(sol));
            scale /= 2;
            continue;
        }

        if (sol[fuel] == 1) {
            part1 = funds - sol[ore];
        }

        scale = 2 * scale;
    }

    printf("%" PRIi64 "\n", part1);
    printf("%" PRIi64 "\n", sol[fuel]);
    return 0;
}
