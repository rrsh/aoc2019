#include "common.h"

#undef P
#define P(x,y) ((Point){(x),(y)})
#define NORM(p) (ABS(p.x) + ABS(p.y))

#define N 512
typedef struct {int x, y;} Point;
static Point w1[N];
static Point w2[N];

static size_t
read_wire(const char *line, Point v[])
{
    Point p = {0, 0};
    size_t i = 0;
    v[i++] = p;
    int n = 0;
    do {
        char dir;
        int x;
        line += n;
        if (sscanf(line, " %c%d%n", &dir, &x, &n) != 2) {
            assert(!"bad input");
        }
        switch (dir) {
        case 'D': p.y -= x; break;
        case 'U': p.y += x; break;
        case 'L': p.x -= x; break;
        case 'R': p.x += x; break;
        }
        v[i++] = p;
        line += n;
    } while (sscanf(line, " ,%n", &n) != EOF);
    return i;
}

static inline int point_equals(const Point *p1, const Point *p2)
{
    return p1->x == p2->x && p1->y == p2->y;
}

static inline Point
line_dir(Point a, Point b)
{
    if (a.x == b.x) {
        return a.y < b.y ? P(0, 1) : P(0, -1);
    }
    return a.x < b.x ? P(1, 0) : P(-1, 0);
}

#define S 40000

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }
    char line[16<<10];
    size_t na = 0, nb = 0;

    if (fgets(line, sizeof(line), stdin)) {
        na = read_wire(line, w1);
    }

    if (fgets(line, sizeof(line), stdin)) {
        nb = read_wire(line, w2);
    }

    Point inter;
    int part1 = INT_MAX;
    int part2 = INT_MAX;
    size_t len = 0;

    char (*grid)[S] = calloc(S, S);
    int  (*costa_grid)[S] = calloc(sizeof(int [S]), S);
    assert(grid && costa_grid);

    char (*g)[S] = (void *)&grid[S/2][S/2];
    int (*cag)[S] = (void *)&costa_grid[S/2][S/2];

    for (size_t i=1; i<na; i++) {
        Point dir = line_dir(w1[i-1], w1[i]);
        Point p = w1[i-1];
        while (!point_equals(&p, &w1[i])) {
            g[p.y][p.x] = 1;
            cag[p.y][p.x] = len++;
            p.x += dir.x, p.y += dir.y;
        }
    }

    len = 0;

    for (size_t i=1; i<nb; i++) {
        Point dir = line_dir(w2[i-1], w2[i]);
        Point p = w2[i-1];
        p.x += dir.x, p.y += dir.y;
        while (!point_equals(&p, &w2[i])) {
            if (g[p.y][p.x]) {
                if (NORM(p) < part1) {
                    part1 = NORM(p);
                }
                int costb = len;
                int costa = cag[p.y][p.x];
                if (costa + costb < part2) {
                    part2 = costa + costb;
                }
            }
            len = len + 1;
            p.x += dir.x, p.y += dir.y;
        }
    }

    printf("%d\n", part1);
    printf("%d\n", part2);
    return 0;
}
