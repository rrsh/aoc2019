#include "common.h"

#define N ((size_t)1E4)
static Vec2i aster[N];
static Vec2i cpy[N];
static Vec2i nextcycle[N];
static int naster;

static int
parse_line(char line[], int y)
{
    char *p = line, c;
    int n, x = 0;
    while (sscanf(p, " %c%n", &c, &n) == 1) {
        switch (c) {
        case '#':
            aster[naster++] = P(x, y);
            break;
        case '.':
            break;
        default:
            assert(!"bad input");
        }
        p += n;
        x += 1;
    }

done:
    return x;
}

static inline int score(Vec2i v) { return 100 * v.x + v.y;}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }
    char line[256];
    int w = 0, h = 0;

    while (fgets(line, sizeof(line), stdin)) {
        int tmp = parse_line(line, h++);
        if (tmp > w) {
            w = tmp;
        }
    }

    memcpy(cpy, aster, sizeof(Vec2i [naster]));
    int part1 = 0;
    int part2 = 0;

    for (int i=0; i<naster; i++) {
        memcpy(aster, cpy, sizeof(Vec2i [naster]));

        Vec2i pos = aster[i];
        SWAP(aster[0], aster[i]);

        for (int j=1; j<naster; j++) {
            aster[j] = vec2i_sub(aster[j], pos);
        }

        qsort(aster+1, naster-1, sizeof(Vec2i), (cmp_t)vec2i_cmpangle);
        memcpy(nextcycle, aster+1, sizeof(Vec2i [naster-1]));

        int n = naster - 1;
        int nnext = n;
        int cnt = 0;
        int tmp = 0;

        while (nnext) {
            n = nnext, nnext = 0;
            memcpy(aster, nextcycle, sizeof(Vec2i [n]));
            for (int j=0; j<n; j++) {
                if (j && !vec2i_cmpangle(&aster[j], &aster[j-1])) {
                    nextcycle[nnext++] = aster[j];
                    continue;
                }
                if (++cnt == 200) {
                    tmp = score(vec2i_add(aster[j], pos));
                }
            }
            if (n == naster-1) {
                if (cnt < part1) {
                    break;
                }
                part1 = cnt;
                part2 = tmp;
            }
        }
    }

    printf("%d\n", part1);
    printf("%d\n", part2);
    return 0;
}
