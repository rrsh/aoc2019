#include "intcode.h"

#define UPDATE(c,i,b) ((c)->ip = (i), (c)->bp = (b))

int
vm_init(struct vm_ctx *ctx, const i64 *mem, size_t n, vm_read_t readcb, vm_write_t writecb)
{
    memcpy(ctx->mem, mem, sizeof(i64 [n]));
    ctx->ip = ctx->mem;
    ctx->state = VM_SRUNNING;
    ctx->readcb = readcb;
    ctx->writecb = writecb;
    ctx->inlen = 0;
    ctx->outlen = 0;
    return 0;
}

int
vm_run(struct vm_ctx *ctx)
{
    i64 *ip = ctx->ip;
    i64 *mem = ctx->mem;
    i64 bp = ctx->bp;
    i64 val = 0;
    int reason = VM_EHALTED;
    int in = 0;
    int out = ctx->outlen;

    while (1) {
        /* see day5-gen.py */
        switch (*ip) {
#include "day5_interp.gen"
        /* end gen code */
        case 3:
            if (in >= ctx->inlen) {
                reason = VM_EINPUT;
                goto stop;
            }
            if (ctx->readcb) {
                UPDATE(ctx, ip, bp);
                reason = ctx->readcb(ctx, mem[ip[1]]);
                if (reason) {
                    goto stop;
                }
            }
            mem[ip[1]] = ctx->qinput[in++];
            ip = ip + 2;
            break;
        case 203:
            if (in >= ctx->inlen) {
                reason = VM_EINPUT;
                goto stop;
            }
            if (ctx->readcb) {
                UPDATE(ctx, ip, bp);
                reason = ctx->readcb(ctx, mem[bp + ip[1]]);
                if (reason) {
                    goto stop;
                }
            }
            mem[bp + ip[1]] = ctx->qinput[in++];
            ip = ip + 2;
            break;
        case 4:
            val = mem[ip[1]];
            goto generic_out;
        case 104:
            val = ip[1];
            goto generic_out;
        case 204:
            val = mem[bp + ip[1]];
            goto generic_out;
generic_out:
            if (out >= VM_MAXOUTPUT) {
                reason = VM_EOUTPUT;
                goto stop;
            }
            if (ctx->writecb) {
                UPDATE(ctx, ip, bp);
                if ((reason = ctx->writecb(ctx, val))) {
                    goto stop;
                }
            }
            ctx->qoutput[out++] = val;
            ip = ip + 2;
            break;
        case 99:
            reason = VM_EHALTED;
            ctx->state = VM_SHALTED;
            goto stop;
        default:
            assert(!"bad instruction");
        }
    }

stop:
    ctx->bp = bp;
    ctx->ip = ip;
    memmove(ctx->qinput, ctx->qinput + in, sizeof(i64 [ctx->inlen - in]));
    ctx->inlen -= in;
    ctx->outlen = out;
    return reason;
}

int vm_push_input(struct vm_ctx *ctx, i64 in)
{
    if (ctx->inlen >= VM_MAXINPUT) {
        return -1;
    }
    ctx->qinput[ctx->inlen++] = in;
    return 0;
}

int vm_pop_output(struct vm_ctx *ctx, i64 *out)
{
    if (!ctx->outlen) {
        return -1;
    }
    *out = ctx->qoutput[--ctx->outlen];
    return 0;
}
