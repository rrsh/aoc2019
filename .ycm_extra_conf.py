#!/usr/bin/env python3

def Settings(**kwargs):
    return {'flags': [ '-x', 'c', '-Wall', '-Wextra', '-Wno-unused']}

if __name__ == '__main__':
    print(Settings())
