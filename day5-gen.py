#!/usr/bin/env python3
from itertools import product

NREGS = 3
OPTABLE = [
#   op len template regs
    (1, 4, '{%d} = {%d} + {%d};', (2,0,1)),
    (2, 4, '{%d} = {%d} * {%d};', (2,0,1)),
    (5, 3, 'if ({%d}) {{ ip = mem + {%d}; break;}}', (0,1)),
    (6, 3, 'if (!{%d}) {{ ip = mem + {%d}; break;}}', (0,1)),
    (7, 4, '{%d} = ({%d} < {%d});', (2,0,1)),
    (8, 4, '{%d} = ({%d} == {%d});', (2,0,1)),
    (9, 2, 'bp = bp + {%d};', (0,))
]

def make_arg(mode, i):
    return ["mem[ip[%d]]", "ip[%d]", "mem[bp + ip[%d]]"][mode] % i

def make_instruction(amode, op):
    return 100 * (100 * amode[2] + 10 * amode[1] + amode[0]) + op;

def is_valid(op, amode, regs):
    if amode[2] == 1:
        return False
    for i in range(NREGS):
        if (not i in regs) and amode[i]:
            return False
    return True

if __name__ == '__main__':
    for amode in product(range(NREGS), range(NREGS), range(NREGS)):
        args = [make_arg(amode[i], i+1) for i in range(NREGS)]
        for op, ilen, template, regs in OPTABLE:
            if not is_valid(op, amode, tuple(regs)):
                continue
            print("case %d:" % make_instruction(amode, op))
            print("   ", (template % regs).format(*args))
            print("    ip = ip + %d;" % ilen)
            print("    break;")
