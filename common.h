#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <time.h>
#include <errno.h>
#include <ctype.h>
#include <math.h>

#define P(x,y) ((Vec2i){(x),(y)})
#define EPS 1E-6
typedef int (*cmp_t)(const void *a, const void *b);

typedef uint8_t byte;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t sbyte;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef struct vec2i {int x, y;} Vec2i;
static inline Vec2i vec2i_sub(Vec2i a, Vec2i b) { return P(a.x - b.x, a.y - b.y);}
static inline Vec2i vec2i_add(Vec2i a, Vec2i b) { return P(a.x + b.x, a.y + b.y);}

#define MAX(a,b) ((a) > (b)? (a) : (b))
#define MIN(a,b) ((a) < (b)? (a) : (b))
#define SWAP(a,b) do {typeof(a) _tmp = (a); (a) = (b), (b) = _tmp;} while (0)
#define SIGN(a) ((a)<0? -1 : (a)>0? 1 : 0)
#define ABS(x) ((x)<0?-(x):(x))
#define CEILDIV(a,b) (((a) + (b) - 1) / (b))

static i64 gcd(i64 a, i64 b) { return b? gcd(b, a%b) : a;}

static inline Vec2i
vec2i_normalize_taxi(Vec2i v)
{
    const int g = gcd(ABS(v.x), ABS(v.y));
    return g? P(v.x/g, v.y/g) : v;
}

static inline double
vec2i_angle(Vec2i v)
{
    if (v.x < 0) {
        return M_PI + atan2(-v.x, v.y);
    }
    return atan2(v.x, -v.y);
}

static inline int
vec2i_cmpangle(const Vec2i *a, const Vec2i *b)
{
    const double dif = vec2i_angle(*a) - vec2i_angle(*b);
    return ABS(dif) < EPS? 0 : dif < 0.? -1 : 1;
}

static inline int
vec2i_cmpdist(const Vec2i *a, const Vec2i *b)
{
    const int da = ABS(a->x) + ABS(a->y);
    const int db = ABS(b->x) + ABS(b->y);
    return da < db? -1 : da > db? 1 : 0;
}

static inline Vec2i
vec2i_fromdir(int d)
{
    switch (d) {
    case 0: return P(0,1);
    case 1: return P(1,0);
    case 2: return P(0,-1);
    case 3: return P(-1,0);
    }
    return P(0,0);
}

#define VEC3I(a,b,c) ((Vec3i){{(a),(b),(c)}})
typedef union vec3i {
    _Alignas(16) struct {
        i32 x, y, z;
    };
    _Alignas(16) i32 v[3];
} Vec3i;
static inline Vec3i vec3i_sub(Vec3i a, Vec3i b) { return VEC3I(a.x - b.x, a.y - b.y, a.z - b.z);}
static inline Vec3i vec3i_add(Vec3i a, Vec3i b) { return VEC3I(a.x + b.x, a.y + b.y, a.z + b.z);}
static inline int vec3i_taxinorm(Vec3i a) {return ABS(a.x) + ABS(a.y) + ABS(a.z);}
