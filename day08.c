#include "common.h"

#define PUT_U32LE(b,x) (*(u32 *)(p) = (x), (p) += 4)
#define PUT_U16LE(b,x) (*(u16 *)(b) = (x), (p) += 2)
enum {W = 25, H = 6, NSAMPLE=16};

static int
dump_bmp(const char *file, const char img[][W])
{
    byte buf[10 << 10];
    FILE *f = fopen(file, "wb");
    if (!f) {
        fprintf(stderr, "fopen('%s'): %s\n", file, strerror(errno));
        return 1;
    }
    byte *p = buf;

    /* BM header */
    PUT_U16LE(p, ('M'<<8)|'B'); // magic
    byte *offsz = p;
    PUT_U32LE(p, 0);            // size of file
    PUT_U16LE(p, 0);            // reserved
    PUT_U16LE(p, 0);            // reserved
    byte *offbm = p;
    PUT_U32LE(p, 0);            // off start of pixels

    /* DIB header */
    PUT_U32LE(p, 40); // size of header
    PUT_U32LE(p,  W*NSAMPLE); // width
    PUT_U32LE(p,  H*NSAMPLE); // height
    PUT_U16LE(p,  1); // color planes
    PUT_U16LE(p, 32); // bpp
    PUT_U32LE(p,  0); // compression
    PUT_U32LE(p,  0); // image sz
    PUT_U32LE(p,  0); // hres
    PUT_U32LE(p,  0); // vres
    PUT_U32LE(p,  0); // num palette colors
    PUT_U32LE(p,  0); // num important colors
    PUT_U32LE(offbm, (p-buf));
    PUT_U32LE(offsz, (p-buf) + 4 * W * H * NSAMPLE * NSAMPLE);

    fwrite(buf, p-buf, 1, f);

    for (int i=0; i<H; i++) {
        p = buf;
        for (int j=0; j<W; j++) {
            const u32 px = img[H-1-i][j] == '0'? 0u : ~0u;
            for (int k=0; k<NSAMPLE; k++) {
                PUT_U32LE(p, px);
            }
        }
        for (int k=0; k<NSAMPLE; k++)
            fwrite(buf, p-buf, 1, f);
    }

    if (ferror(f)) {
        perror("fwrite() failed");
        goto end;
    }
end:
    return fclose(f);
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }
    int i, j;
    int part1 = 0;
    int min = INT_MAX;

    char img[H][W];
    memset(img, '2', sizeof(img));

    while (1) {
        int freq[10] = {0}, c;
        for (i=0; i<H; i++) {
            for (j=0; j<W; j++) {
                if (!isdigit((c = fgetc(stdin)))) {
                    goto done;
                }
                freq[c - '0']++;
                if (img[i][j] == '2') img[i][j] = c;
            }
        }
        if (freq[0] < min) {
            min = freq[0];
            part1 = freq[1] * freq[2];
        }
    }

done:
    if (i || j) {
        assert(!"bad input");
    }
    printf("%d\n", part1);

    const char *file = argc>2? argv[2] : "out.bmp";
    return dump_bmp(file, img);
}
