CC ?= gcc
CFLAGS += -Wall -Wextra -Wshadow -Wno-unused -Wno-unknown-pragmas
DAYS :=   \
    day01 \
    day02 \
    day03 \
    day04 \
    day05 \
    day06 \
    day07 \
    day08 \
    day09 \
    day10 \
    day11 \
    day12 \
    day13 \
    day14
LIBS := -lm
DEBUG ?= 1

ifeq ($(DEBUG), 1)
    CFLAGS += -g3 -fsanitize=address -ftrapv
else
    CFLAGS += -Ofast -march=native -DNDEBUG -fopenmp
endif

all: $(DAYS)

$(DAYS): %: %.c
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

day05.c: day5_interp.gen
day5_interp.gen: day5-gen.py
	python3 $< > $@

day07.c: day7_perms.gen
day7_perms.gen: day7-gen-perms.py
	python3 $< > $@

day09.c: day5_interp.gen

intcode.c: day5_interp.gen
day11: intcode.c
day13: intcode.c

.PHONY: all clean
clean:
	rm -f $(DAYS)
	rm *.gen

