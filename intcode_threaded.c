#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t sbyte;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#define N ((size_t)1E5)
static i64 program[N];

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    unsigned long long x;

    do {
        if (fscanf(f, "%lld", &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < N);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

#define INSTR(c) instr##c
#define DISPATCH(ip) goto *LABELS[*ip];

static i64 *
run_program(i64 *mem, size_t n, i64 *in)
{
    static void * const LABELS[] = {
        [   1] = &&INSTR(1),
        [   2] = &&INSTR(2),
        [   3] = &&INSTR(3),
        [   4] = &&INSTR(4),
        [   5] = &&INSTR(5),
        [   6] = &&INSTR(6),
        [   7] = &&INSTR(7),
        [   8] = &&INSTR(8),
        [  99] = &&INSTR(99),
        [ 101] = &&INSTR(101),
        [ 102] = &&INSTR(102),
        [ 105] = &&INSTR(105),
        [ 104] = &&INSTR(104),
        [ 106] = &&INSTR(106),
        [ 107] = &&INSTR(107),
        [ 108] = &&INSTR(108),
        [1001] = &&INSTR(1001),
        [1002] = &&INSTR(1002),
        [1005] = &&INSTR(1005),
        [1006] = &&INSTR(1006),
        [1007] = &&INSTR(1007),
        [1008] = &&INSTR(1008),
        [1101] = &&INSTR(1101),
        [1102] = &&INSTR(1102),
        [1105] = &&INSTR(1105),
        [1106] = &&INSTR(1106),
        [1107] = &&INSTR(1107),
        [1108] = &&INSTR(1108),
    };
    i64 *ip = mem;
    DISPATCH(ip);
    /* begin gen code  */
INSTR(1):
    mem[ip[3]] = (mem[ip[1]] + mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(2):
    mem[ip[3]] = (mem[ip[1]] * mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(5):
    if (mem[ip[1]]) { ip = mem + mem[ip[2]]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(6):
    if (!mem[ip[1]]) { ip = mem + mem[ip[2]]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(7):
    mem[ip[3]] = (mem[ip[1]] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(8):
    mem[ip[3]] = (mem[ip[1]] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(101):
    mem[ip[3]] = (ip[1] + mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(102):
    mem[ip[3]] = (ip[1] * mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(105):
    if (ip[1]) { ip = mem + mem[ip[2]]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(106):
    if (!ip[1]) { ip = mem + mem[ip[2]]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(107):
    mem[ip[3]] = (ip[1] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(108):
    mem[ip[3]] = (ip[1] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1001):
    mem[ip[3]] = (mem[ip[1]] + ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1002):
    mem[ip[3]] = (mem[ip[1]] * ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1005):
    if (mem[ip[1]]) { ip = mem + ip[2]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(1006):
    if (!mem[ip[1]]) { ip = mem + ip[2]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(1007):
    mem[ip[3]] = (mem[ip[1]] < ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1008):
    mem[ip[3]] = (mem[ip[1]] == ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1101):
    mem[ip[3]] = (ip[1] + ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1102):
    mem[ip[3]] = (ip[1] * ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1105):
    if (ip[1]) { ip = mem + ip[2]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(1106):
    if (!ip[1]) { ip = mem + ip[2]; DISPATCH(ip);}
    ip = ip + 3;
    DISPATCH(ip);
INSTR(1107):
    mem[ip[3]] = (ip[1] < ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
INSTR(1108):
    mem[ip[3]] = (ip[1] == ip[2]);
    ip = ip + 4;
    DISPATCH(ip);
    /* end gen code */
INSTR(3):
    mem[ip[1]] = *in++;
    ip = ip + 2;
    DISPATCH(ip);
INSTR(104):
    printf("%" PRIi64 "\n",ip[1]);
    ip = ip + 2;
    DISPATCH(ip);
INSTR(4):
    printf("%" PRIi64 "\n", mem[ip[1]]);
    ip = ip + 2;
    DISPATCH(ip);
INSTR(99):
    goto halt;
halt:
    return ip;
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    size_t n = 0;

    read_program(program, &n, stdin);

    i64 in[] = {1234567890};
    run_program(program, n, in);

    return 0;
}
