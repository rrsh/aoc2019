#include "common.h"

static int
incrdig(int x)
{
    int prev = 9;
    while (x) {
        int next = x % 10;
        if (next > prev) { return false;}
        x = x / 10, prev = next;
    }
    return true;
}

static int
adjdig(int x)
{
    int prev = -1;
    while (x) {
        int next = x % 10;
        if (next == prev) return true;
        x = x / 10, prev = next;
    }
    return false;
}

static int
adjdig2(int x)
{
    int prev = -1;
    int cnt = 1;
    bool hasgroup = 0;
    while (x) {
        int next = x % 10;
        if (next == prev) {
            cnt++;
        } else {
            hasgroup |= (cnt == 2);
            cnt = 1;
        }
        x = x / 10, prev = next;
    }
    return hasgroup || (cnt == 2);
}

int main(void)
{
    int a, b;
    if (scanf("%d - %d", &a, &b) != 2) {
        assert(!"bad input");
    }

    int part1 = 0;
    int part2 = 0;

    for (int i=a; i<=b; i++) {
        if (incrdig(i)) {
            part1 += adjdig(i);
            part2 += adjdig2(i);
        }
    }

    printf("%d\n", part1);
    printf("%d\n", part2);
    return 0;
}

