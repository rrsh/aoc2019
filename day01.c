#include "common.h"

static int fuel(int x, int acc) { return x < 1? acc : fuel(x/3-2, acc + x);}

int main(void)
{
    int x, sum = 0;
    while (scanf("%d", &x) == 1) {
        sum += fuel(x/3-2, 0);
    }
    printf("%d\n", sum);
    return 0;
}
