#!/usr/bin/env python3
from itertools import permutations
if __name__ == '__main__':
    print('#define PERM5_LIST(XX) \\')
    for perm in permutations(range(5)):
        print('    XX(%d,%d,%d,%d,%d) \\' % perm)
    print('')
