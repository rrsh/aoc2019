#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

typedef uint8_t byte;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t sbyte;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#define N ((size_t)1E5)
static i64 program[N];
static i64 cpy[N];

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    i64 x;

    do {
        if (fscanf(f, "%" SCNi64 , &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < N);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

#define INSTR(c) instr##c
#define DISPATCH(i) goto *LABELS[i];

#ifndef USESWITCH
#  define vm_run(...) (vm_interp_threaded)(__VA_ARGS__)
#else
#  define vm_run(...) (vm_interp_switch)(__VA_ARGS__)
#endif

#define vm_input(in) (*(in)++)
#define vm_output(out) //printf("%" PRIi64 "\n", (out))

static i64 *
vm_interp_threaded(i64 *mem, size_t n, i64 *in)
{
    (void) n;
    static void * const LABELS[] = {
        [1] = &&INSTR(1),
        [2] = &&INSTR(2),
        [5] = &&INSTR(5),
        [6] = &&INSTR(6),
        [7] = &&INSTR(7),
        [8] = &&INSTR(8),
        [9] = &&INSTR(9),
        [20001] = &&INSTR(20001),
        [20002] = &&INSTR(20002),
        [20007] = &&INSTR(20007),
        [20008] = &&INSTR(20008),
        [1001] = &&INSTR(1001),
        [1002] = &&INSTR(1002),
        [1005] = &&INSTR(1005),
        [1006] = &&INSTR(1006),
        [1007] = &&INSTR(1007),
        [1008] = &&INSTR(1008),
        [21001] = &&INSTR(21001),
        [21002] = &&INSTR(21002),
        [21007] = &&INSTR(21007),
        [21008] = &&INSTR(21008),
        [2001] = &&INSTR(2001),
        [2002] = &&INSTR(2002),
        [2005] = &&INSTR(2005),
        [2006] = &&INSTR(2006),
        [2007] = &&INSTR(2007),
        [2008] = &&INSTR(2008),
        [22001] = &&INSTR(22001),
        [22002] = &&INSTR(22002),
        [22007] = &&INSTR(22007),
        [22008] = &&INSTR(22008),
        [101] = &&INSTR(101),
        [102] = &&INSTR(102),
        [105] = &&INSTR(105),
        [106] = &&INSTR(106),
        [107] = &&INSTR(107),
        [108] = &&INSTR(108),
        [109] = &&INSTR(109),
        [20101] = &&INSTR(20101),
        [20102] = &&INSTR(20102),
        [20107] = &&INSTR(20107),
        [20108] = &&INSTR(20108),
        [1101] = &&INSTR(1101),
        [1102] = &&INSTR(1102),
        [1105] = &&INSTR(1105),
        [1106] = &&INSTR(1106),
        [1107] = &&INSTR(1107),
        [1108] = &&INSTR(1108),
        [21101] = &&INSTR(21101),
        [21102] = &&INSTR(21102),
        [21107] = &&INSTR(21107),
        [21108] = &&INSTR(21108),
        [2101] = &&INSTR(2101),
        [2102] = &&INSTR(2102),
        [2105] = &&INSTR(2105),
        [2106] = &&INSTR(2106),
        [2107] = &&INSTR(2107),
        [2108] = &&INSTR(2108),
        [22101] = &&INSTR(22101),
        [22102] = &&INSTR(22102),
        [22107] = &&INSTR(22107),
        [22108] = &&INSTR(22108),
        [201] = &&INSTR(201),
        [202] = &&INSTR(202),
        [205] = &&INSTR(205),
        [206] = &&INSTR(206),
        [207] = &&INSTR(207),
        [208] = &&INSTR(208),
        [209] = &&INSTR(209),
        [20201] = &&INSTR(20201),
        [20202] = &&INSTR(20202),
        [20207] = &&INSTR(20207),
        [20208] = &&INSTR(20208),
        [1201] = &&INSTR(1201),
        [1202] = &&INSTR(1202),
        [1205] = &&INSTR(1205),
        [1206] = &&INSTR(1206),
        [1207] = &&INSTR(1207),
        [1208] = &&INSTR(1208),
        [21201] = &&INSTR(21201),
        [21202] = &&INSTR(21202),
        [21207] = &&INSTR(21207),
        [21208] = &&INSTR(21208),
        [2201] = &&INSTR(2201),
        [2202] = &&INSTR(2202),
        [2205] = &&INSTR(2205),
        [2206] = &&INSTR(2206),
        [2207] = &&INSTR(2207),
        [2208] = &&INSTR(2208),
        [22201] = &&INSTR(22201),
        [22202] = &&INSTR(22202),
        [22207] = &&INSTR(22207),
        [22208] = &&INSTR(22208),
        [3] = &&INSTR(3),
        [203] = &&INSTR(203),
        [4] = &&INSTR(4),
        [104] = &&INSTR(104),
        [204] = &&INSTR(204),
        [99] = &&INSTR(99),
    };
    i64 *ip = mem;
    i64 bp = 0;
    DISPATCH(*ip);
    /* begin gen code  */
INSTR(1):
    mem[ip[3]] = mem[ip[1]] + mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2):
    mem[ip[3]] = mem[ip[1]] * mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(5):
    if (mem[ip[1]]) { ip = mem + mem[ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(6):
    if (!mem[ip[1]]) { ip = mem + mem[ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(7):
    mem[ip[3]] = (mem[ip[1]] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(8):
    mem[ip[3]] = (mem[ip[1]] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(9):
    bp = bp + mem[ip[1]];
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(20001):
    mem[bp + ip[3]] = mem[ip[1]] + mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20002):
    mem[bp + ip[3]] = mem[ip[1]] * mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20007):
    mem[bp + ip[3]] = (mem[ip[1]] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20008):
    mem[bp + ip[3]] = (mem[ip[1]] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1001):
    mem[ip[3]] = mem[ip[1]] + ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1002):
    mem[ip[3]] = mem[ip[1]] * ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1005):
    if (mem[ip[1]]) { ip = mem + ip[2]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(1006):
    if (!mem[ip[1]]) { ip = mem + ip[2]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(1007):
    mem[ip[3]] = (mem[ip[1]] < ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1008):
    mem[ip[3]] = (mem[ip[1]] == ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21001):
    mem[bp + ip[3]] = mem[ip[1]] + ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21002):
    mem[bp + ip[3]] = mem[ip[1]] * ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21007):
    mem[bp + ip[3]] = (mem[ip[1]] < ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21008):
    mem[bp + ip[3]] = (mem[ip[1]] == ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2001):
    mem[ip[3]] = mem[ip[1]] + mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2002):
    mem[ip[3]] = mem[ip[1]] * mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2005):
    if (mem[ip[1]]) { ip = mem + mem[bp + ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(2006):
    if (!mem[ip[1]]) { ip = mem + mem[bp + ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(2007):
    mem[ip[3]] = (mem[ip[1]] < mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2008):
    mem[ip[3]] = (mem[ip[1]] == mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22001):
    mem[bp + ip[3]] = mem[ip[1]] + mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22002):
    mem[bp + ip[3]] = mem[ip[1]] * mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22007):
    mem[bp + ip[3]] = (mem[ip[1]] < mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22008):
    mem[bp + ip[3]] = (mem[ip[1]] == mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(101):
    mem[ip[3]] = ip[1] + mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(102):
    mem[ip[3]] = ip[1] * mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(105):
    if (ip[1]) { ip = mem + mem[ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(106):
    if (!ip[1]) { ip = mem + mem[ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(107):
    mem[ip[3]] = (ip[1] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(108):
    mem[ip[3]] = (ip[1] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(109):
    bp = bp + ip[1];
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(20101):
    mem[bp + ip[3]] = ip[1] + mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20102):
    mem[bp + ip[3]] = ip[1] * mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20107):
    mem[bp + ip[3]] = (ip[1] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20108):
    mem[bp + ip[3]] = (ip[1] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1101):
    mem[ip[3]] = ip[1] + ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1102):
    mem[ip[3]] = ip[1] * ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1105):
    if (ip[1]) { ip = mem + ip[2]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(1106):
    if (!ip[1]) { ip = mem + ip[2]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(1107):
    mem[ip[3]] = (ip[1] < ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1108):
    mem[ip[3]] = (ip[1] == ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21101):
    mem[bp + ip[3]] = ip[1] + ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21102):
    mem[bp + ip[3]] = ip[1] * ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21107):
    mem[bp + ip[3]] = (ip[1] < ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21108):
    mem[bp + ip[3]] = (ip[1] == ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2101):
    mem[ip[3]] = ip[1] + mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2102):
    mem[ip[3]] = ip[1] * mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2105):
    if (ip[1]) { ip = mem + mem[bp + ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(2106):
    if (!ip[1]) { ip = mem + mem[bp + ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(2107):
    mem[ip[3]] = (ip[1] < mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2108):
    mem[ip[3]] = (ip[1] == mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22101):
    mem[bp + ip[3]] = ip[1] + mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22102):
    mem[bp + ip[3]] = ip[1] * mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22107):
    mem[bp + ip[3]] = (ip[1] < mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22108):
    mem[bp + ip[3]] = (ip[1] == mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(201):
    mem[ip[3]] = mem[bp + ip[1]] + mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(202):
    mem[ip[3]] = mem[bp + ip[1]] * mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(205):
    if (mem[bp + ip[1]]) { ip = mem + mem[ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(206):
    if (!mem[bp + ip[1]]) { ip = mem + mem[ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(207):
    mem[ip[3]] = (mem[bp + ip[1]] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(208):
    mem[ip[3]] = (mem[bp + ip[1]] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(209):
    bp = bp + mem[bp + ip[1]];
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(20201):
    mem[bp + ip[3]] = mem[bp + ip[1]] + mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20202):
    mem[bp + ip[3]] = mem[bp + ip[1]] * mem[ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20207):
    mem[bp + ip[3]] = (mem[bp + ip[1]] < mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(20208):
    mem[bp + ip[3]] = (mem[bp + ip[1]] == mem[ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1201):
    mem[ip[3]] = mem[bp + ip[1]] + ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1202):
    mem[ip[3]] = mem[bp + ip[1]] * ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1205):
    if (mem[bp + ip[1]]) { ip = mem + ip[2]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(1206):
    if (!mem[bp + ip[1]]) { ip = mem + ip[2]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(1207):
    mem[ip[3]] = (mem[bp + ip[1]] < ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(1208):
    mem[ip[3]] = (mem[bp + ip[1]] == ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21201):
    mem[bp + ip[3]] = mem[bp + ip[1]] + ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21202):
    mem[bp + ip[3]] = mem[bp + ip[1]] * ip[2];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21207):
    mem[bp + ip[3]] = (mem[bp + ip[1]] < ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(21208):
    mem[bp + ip[3]] = (mem[bp + ip[1]] == ip[2]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2201):
    mem[ip[3]] = mem[bp + ip[1]] + mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2202):
    mem[ip[3]] = mem[bp + ip[1]] * mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2205):
    if (mem[bp + ip[1]]) { ip = mem + mem[bp + ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(2206):
    if (!mem[bp + ip[1]]) { ip = mem + mem[bp + ip[2]]; DISPATCH(*ip);}
    ip = ip + 3;
    DISPATCH(*ip);
INSTR(2207):
    mem[ip[3]] = (mem[bp + ip[1]] < mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(2208):
    mem[ip[3]] = (mem[bp + ip[1]] == mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22201):
    mem[bp + ip[3]] = mem[bp + ip[1]] + mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22202):
    mem[bp + ip[3]] = mem[bp + ip[1]] * mem[bp + ip[2]];
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22207):
    mem[bp + ip[3]] = (mem[bp + ip[1]] < mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
INSTR(22208):
    mem[bp + ip[3]] = (mem[bp + ip[1]] == mem[bp + ip[2]]);
    ip = ip + 4;
    DISPATCH(*ip);
    /* end gen code */
INSTR(3):
    mem[ip[1]] = vm_input(in);
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(203):
    mem[bp + ip[1]] = vm_input(in);
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(4):
    vm_output(mem[ip[1]]);
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(104):
    vm_output(ip[1]);
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(204):
    vm_output(mem[bp + ip[1]]);
    ip = ip + 2;
    DISPATCH(*ip);
INSTR(99):
    goto halt;
halt:
    return ip;
}

static i64 *
vm_interp_switch(i64 *mem, size_t n, i64 *in)
{
    i64 *ip = mem;
    i64 bp = 0;

    while (1) {
        assert((size_t)(ip - mem) < n);
        /* see day5-gen.py */
        switch (*ip) {
        /* begin gen code */
        case 1:
            mem[ip[3]] = mem[ip[1]] + mem[ip[2]];
            ip = ip + 4;
            break;
        case 2:
            mem[ip[3]] = mem[ip[1]] * mem[ip[2]];
            ip = ip + 4;
            break;
        case 5:
            if (mem[ip[1]]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 6:
            if (!mem[ip[1]]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 7:
            mem[ip[3]] = (mem[ip[1]] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 8:
            mem[ip[3]] = (mem[ip[1]] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 9:
            bp = bp + mem[ip[1]];
            ip = ip + 2;
            break;
        case 20001:
            mem[bp + ip[3]] = mem[ip[1]] + mem[ip[2]];
            ip = ip + 4;
            break;
        case 20002:
            mem[bp + ip[3]] = mem[ip[1]] * mem[ip[2]];
            ip = ip + 4;
            break;
        case 20007:
            mem[bp + ip[3]] = (mem[ip[1]] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 20008:
            mem[bp + ip[3]] = (mem[ip[1]] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 1001:
            mem[ip[3]] = mem[ip[1]] + ip[2];
            ip = ip + 4;
            break;
        case 1002:
            mem[ip[3]] = mem[ip[1]] * ip[2];
            ip = ip + 4;
            break;
        case 1005:
            if (mem[ip[1]]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1006:
            if (!mem[ip[1]]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1007:
            mem[ip[3]] = (mem[ip[1]] < ip[2]);
            ip = ip + 4;
            break;
        case 1008:
            mem[ip[3]] = (mem[ip[1]] == ip[2]);
            ip = ip + 4;
            break;
        case 21001:
            mem[bp + ip[3]] = mem[ip[1]] + ip[2];
            ip = ip + 4;
            break;
        case 21002:
            mem[bp + ip[3]] = mem[ip[1]] * ip[2];
            ip = ip + 4;
            break;
        case 21007:
            mem[bp + ip[3]] = (mem[ip[1]] < ip[2]);
            ip = ip + 4;
            break;
        case 21008:
            mem[bp + ip[3]] = (mem[ip[1]] == ip[2]);
            ip = ip + 4;
            break;
        case 2001:
            mem[ip[3]] = mem[ip[1]] + mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 2002:
            mem[ip[3]] = mem[ip[1]] * mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 2005:
            if (mem[ip[1]]) { ip = mem + mem[bp + ip[2]]; break;}
            ip = ip + 3;
            break;
        case 2006:
            if (!mem[ip[1]]) { ip = mem + mem[bp + ip[2]]; break;}
            ip = ip + 3;
            break;
        case 2007:
            mem[ip[3]] = (mem[ip[1]] < mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 2008:
            mem[ip[3]] = (mem[ip[1]] == mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 22001:
            mem[bp + ip[3]] = mem[ip[1]] + mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 22002:
            mem[bp + ip[3]] = mem[ip[1]] * mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 22007:
            mem[bp + ip[3]] = (mem[ip[1]] < mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 22008:
            mem[bp + ip[3]] = (mem[ip[1]] == mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 101:
            mem[ip[3]] = ip[1] + mem[ip[2]];
            ip = ip + 4;
            break;
        case 102:
            mem[ip[3]] = ip[1] * mem[ip[2]];
            ip = ip + 4;
            break;
        case 105:
            if (ip[1]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 106:
            if (!ip[1]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 107:
            mem[ip[3]] = (ip[1] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 108:
            mem[ip[3]] = (ip[1] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 109:
            bp = bp + ip[1];
            ip = ip + 2;
            break;
        case 20101:
            mem[bp + ip[3]] = ip[1] + mem[ip[2]];
            ip = ip + 4;
            break;
        case 20102:
            mem[bp + ip[3]] = ip[1] * mem[ip[2]];
            ip = ip + 4;
            break;
        case 20107:
            mem[bp + ip[3]] = (ip[1] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 20108:
            mem[bp + ip[3]] = (ip[1] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 1101:
            mem[ip[3]] = ip[1] + ip[2];
            ip = ip + 4;
            break;
        case 1102:
            mem[ip[3]] = ip[1] * ip[2];
            ip = ip + 4;
            break;
        case 1105:
            if (ip[1]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1106:
            if (!ip[1]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1107:
            mem[ip[3]] = (ip[1] < ip[2]);
            ip = ip + 4;
            break;
        case 1108:
            mem[ip[3]] = (ip[1] == ip[2]);
            ip = ip + 4;
            break;
        case 21101:
            mem[bp + ip[3]] = ip[1] + ip[2];
            ip = ip + 4;
            break;
        case 21102:
            mem[bp + ip[3]] = ip[1] * ip[2];
            ip = ip + 4;
            break;
        case 21107:
            mem[bp + ip[3]] = (ip[1] < ip[2]);
            ip = ip + 4;
            break;
        case 21108:
            mem[bp + ip[3]] = (ip[1] == ip[2]);
            ip = ip + 4;
            break;
        case 2101:
            mem[ip[3]] = ip[1] + mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 2102:
            mem[ip[3]] = ip[1] * mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 2105:
            if (ip[1]) { ip = mem + mem[bp + ip[2]]; break;}
            ip = ip + 3;
            break;
        case 2106:
            if (!ip[1]) { ip = mem + mem[bp + ip[2]]; break;}
            ip = ip + 3;
            break;
        case 2107:
            mem[ip[3]] = (ip[1] < mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 2108:
            mem[ip[3]] = (ip[1] == mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 22101:
            mem[bp + ip[3]] = ip[1] + mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 22102:
            mem[bp + ip[3]] = ip[1] * mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 22107:
            mem[bp + ip[3]] = (ip[1] < mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 22108:
            mem[bp + ip[3]] = (ip[1] == mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 201:
            mem[ip[3]] = mem[bp + ip[1]] + mem[ip[2]];
            ip = ip + 4;
            break;
        case 202:
            mem[ip[3]] = mem[bp + ip[1]] * mem[ip[2]];
            ip = ip + 4;
            break;
        case 205:
            if (mem[bp + ip[1]]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 206:
            if (!mem[bp + ip[1]]) { ip = mem + mem[ip[2]]; break;}
            ip = ip + 3;
            break;
        case 207:
            mem[ip[3]] = (mem[bp + ip[1]] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 208:
            mem[ip[3]] = (mem[bp + ip[1]] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 209:
            bp = bp + mem[bp + ip[1]];
            ip = ip + 2;
            break;
        case 20201:
            mem[bp + ip[3]] = mem[bp + ip[1]] + mem[ip[2]];
            ip = ip + 4;
            break;
        case 20202:
            mem[bp + ip[3]] = mem[bp + ip[1]] * mem[ip[2]];
            ip = ip + 4;
            break;
        case 20207:
            mem[bp + ip[3]] = (mem[bp + ip[1]] < mem[ip[2]]);
            ip = ip + 4;
            break;
        case 20208:
            mem[bp + ip[3]] = (mem[bp + ip[1]] == mem[ip[2]]);
            ip = ip + 4;
            break;
        case 1201:
            mem[ip[3]] = mem[bp + ip[1]] + ip[2];
            ip = ip + 4;
            break;
        case 1202:
            mem[ip[3]] = mem[bp + ip[1]] * ip[2];
            ip = ip + 4;
            break;
        case 1205:
            if (mem[bp + ip[1]]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1206:
            if (!mem[bp + ip[1]]) { ip = mem + ip[2]; break;}
            ip = ip + 3;
            break;
        case 1207:
            mem[ip[3]] = (mem[bp + ip[1]] < ip[2]);
            ip = ip + 4;
            break;
        case 1208:
            mem[ip[3]] = (mem[bp + ip[1]] == ip[2]);
            ip = ip + 4;
            break;
        case 21201:
            mem[bp + ip[3]] = mem[bp + ip[1]] + ip[2];
            ip = ip + 4;
            break;
        case 21202:
            mem[bp + ip[3]] = mem[bp + ip[1]] * ip[2];
            ip = ip + 4;
            break;
        case 21207:
            mem[bp + ip[3]] = (mem[bp + ip[1]] < ip[2]);
            ip = ip + 4;
            break;
        case 21208:
            mem[bp + ip[3]] = (mem[bp + ip[1]] == ip[2]);
            ip = ip + 4;
            break;
        case 2201:
            mem[ip[3]] = mem[bp + ip[1]] + mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 2202:
            mem[ip[3]] = mem[bp + ip[1]] * mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 2205:
            if (mem[bp + ip[1]]) { ip = mem + mem[bp + ip[2]]; break;}
            ip = ip + 3;
            break;
        case 2206:
            if (!mem[bp + ip[1]]) { ip = mem + mem[bp + ip[2]]; break;}
            ip = ip + 3;
            break;
        case 2207:
            mem[ip[3]] = (mem[bp + ip[1]] < mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 2208:
            mem[ip[3]] = (mem[bp + ip[1]] == mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 22201:
            mem[bp + ip[3]] = mem[bp + ip[1]] + mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 22202:
            mem[bp + ip[3]] = mem[bp + ip[1]] * mem[bp + ip[2]];
            ip = ip + 4;
            break;
        case 22207:
            mem[bp + ip[3]] = (mem[bp + ip[1]] < mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        case 22208:
            mem[bp + ip[3]] = (mem[bp + ip[1]] == mem[bp + ip[2]]);
            ip = ip + 4;
            break;
        /* end gen code */
        case 3:
            mem[ip[1]] = vm_input(in);
            ip = ip + 2;
            break;
        case 203:
            mem[bp + ip[1]] = vm_input(in);
            ip = ip + 2;
            break;
        case 4:
            vm_output(mem[ip[1]]);
            ip = ip + 2;
            break;
        case 104:
            vm_output(ip[1]);
            ip = ip + 2;
            break;
        case 204:
            vm_output(mem[bp + ip[1]]);
            ip = ip + 2;
            break;
        case 99:
            goto halt;
        default:
            __builtin_unreachable();
        }
    }

halt:
    return ip;
}

int main(int argc, char *argv[])
{
    (void)vm_interp_threaded;
    (void)vm_interp_switch;
    if (argv[1]) {
        if (!freopen(argv[1], "r", stdin)) {
            assert(!"freopen() failed");
        }
    }

    size_t n = 0;
    read_program(program, &n, stdin);
    memcpy(cpy, program, sizeof(i64 [n]));

    i64 in[1];

    clock_t t0, t1;
    double ttotal;

    int nsample = 2000;
    printf("Running %d times...\n", nsample);

    /* part 1 */
    t0 = clock();
    for (int i=0; i<nsample; i++) {
        /* part 1 */
        memcpy(program, cpy, sizeof(i64 [N]));
        in[0] = 1;
        vm_run(program, n, in);
    }
    t1 = clock();

    ttotal = 1E3 * (t1 - t0) / CLOCKS_PER_SEC;
    ttotal /= nsample;
    double tpart1 = ttotal;

    /* part 2 */
    t0 = clock();
    for (int i=0; i<nsample; i++) {
        memcpy(program, cpy, sizeof(i64 [N]));
        in[0] = 2;
        vm_run(program, n, in);
    }
    t1 = clock();

    ttotal = 1E3 * (t1 - t0) / CLOCKS_PER_SEC;
    ttotal /= nsample;
    double tpart2 = ttotal;

    printf("Part 1: avg  .... %5.2lfms\n", tpart1);
    printf("Part 2: avg  .... %5.2lfms\n", tpart2);
    return 0;
}
