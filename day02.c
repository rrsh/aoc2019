#include "common.h"

#define N ((size_t)1E5)
typedef uint64_t u64;
typedef uint8_t byte;

static u64 program[N];
static u64 cpy[N];
static byte gen[4<<10];

static int
read_program(u64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    unsigned long long x;

    do {
        if (fscanf(f, "%llu", &x) != 1) {
            fputs("bad input", stderr);
            return -1;
        }

        assert(i + 1 < N);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    *len = i + 1;
    return 0;
}

static void
run_program(u64 *src, size_t n)
{
    size_t i = 0;
    bool running = true;

    while (running) {
        assert(i < n);
        u64 a, b, dest;
        if (src[i] != 99) {
            a = src[i+1];
            if (a >= n) { break;}
            b = src[i+2];
            if (b >= n) { break;}
            dest = src[i+3];
            if (dest >= n) { break;}
        }
        switch (src[i]) {
        case 1: src[dest] = src[a] + src[b]; break;
        case 2: src[dest] = src[a] * src[b]; break;
        case 99: running = false; break;
        default: assert(!"bad opcode");
        }
        i = i + 4;
    }
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    size_t n = 0;
    read_program(program, &n, stdin);
    memcpy(cpy, program, sizeof(u64 [n]));

    const u64 limit = 1E5;
    /* part 1 */
    program[1] = 12;
    program[2] = 2;
    run_program(program, n);
    printf("%" PRIu64 "\n", program[0]);

    /* part 2 */
    for (u64 i=0; i<limit; i++) {
        for (u64 j=0; j<limit; j++) {
            memcpy(program, cpy, sizeof(u64 [n]));
            program[1] = i;
            program[2] = j;
            run_program(program, n);
            if (program[0] == 19690720) {
                goto done;
            }
        }
    }

done:
    printf("%" PRIu64 "\n", (100 * program[1] + program[2]));
    return 0;
}
