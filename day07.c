#include "common.h"

#include "day7_perms.gen"
static const i64 PERMS[][5] = {
#define XX(a,b,c,d,e) {a,b,c,d,e},
    PERM5_LIST(XX)
#undef XX
};

#define NPERMS (sizeof(PERMS) / sizeof(PERMS[0]))
#define N ((size_t)1E5)
static i64 program[N];
static size_t n;

enum {
    VM_EVHALT,
    VM_EVWAITINPUT,
    VM_EVOUTOVERFLOW,
};

struct vm_ctx {
    i64 mem[N];
    size_t len;
    i64 *ip;
    bool halted;
};

static struct vm_ctx vmtab[5];

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    unsigned long long x;

    do {
        if (fscanf(f, "%lld", &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < N);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

static void
vm_init(struct vm_ctx *ctx, const i64 *code, size_t len)
{
    memcpy(ctx->mem, code, sizeof(i64 [len]));
    ctx->len = len;
    ctx->ip = ctx->mem;
    ctx->halted = false;
}

static int
vm_run(struct vm_ctx *ctx, i64 *in, size_t inlen, i64 *out, size_t outlen)
{
    i64 *ip = ctx->ip;
    i64 *mem = ctx->mem;
    int reason = VM_EVHALT;

    i64 bp = 0;
    (void)bp;

    if (ctx->halted) {
        assert(!"machine was halted");
    }

    while (1) {
        assert((size_t)(ip - mem) < ctx->len);
        /* see day5-gen.py */
        switch (*ip) {
#include "day5_interp.gen"
        /* end gen code */
        case 3:
            if (!inlen--) {
                reason = VM_EVWAITINPUT;
                goto stop;
            }
            mem[ip[1]] = *in++;
            ip = ip + 2;
            break;
        case 104:
            if (!outlen--) {
                reason = VM_EVOUTOVERFLOW;
                goto stop;
            }
            assert(inlen == 0);
            *out++ = ip[1];
            ip = ip + 2;
            break;
        case 4:
            if (!outlen--) {
                reason = VM_EVOUTOVERFLOW;
                goto stop;
            }
            assert(inlen == 0);
            *out++ = mem[ip[1]];
            ip = ip + 2;
            break;
        case 99:
            reason = VM_EVHALT;
            ctx->halted = true;
            goto stop;
        default:
            printf("[ip] = %d\n", (int)*ip);
            assert(!"bad instruction");
        }
    }

stop:
    ctx->ip = ip;
    return reason;
}

static int
feed_amp_chain(struct vm_ctx vms[], i64 inval, i64 *outval, const i64 phase[5])
{
    i64 in[2];
    i64 out = inval;

    for (int i=0; i<5; i++) {
        if (vms[i].halted) {
            return false;
        }
        /* XXX: hack! */
        if (vms[i].ip == vms[i].mem) {
            in[0] = phase[i];
            in[1] = out;
            vm_run(&vms[i], in, 2, &out, 1);
        } else {
            in[0] = out;
            vm_run(&vms[i], in, 1, &out, 1);
        }
    }

    *outval = out;
    return true;
}

static int
run_feedback_loop(const i64 init[])
{
    i64 input = 0, output = 0;
    struct vm_ctx *vms = malloc(sizeof(vms[0]) * 5);
    for (int i=0; i<5; i++) { vm_init(&vms[i], program, n);}

    while (feed_amp_chain(vms,input,&output,init)) {
        input = output;
    }

    free(vms);
    return input;
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    i64 part1 = 0;
    i64 part2 = 0;
    read_program(program, &n, stdin);

    for (size_t i=0; i<NPERMS; i++) {
        for (int j=0; j<5; j++) {
            vm_init(&vmtab[j], program, n);
        }
        i64 output = 0;
        feed_amp_chain(vmtab,0,&output,PERMS[i]);
        if (output > part1) {
            part1 = output;
        }
    }

    i64 out[NPERMS];
    const clock_t t0 = clock();

#define NSAMPLE 10000
    for (int k=0; k<NSAMPLE; k++) {
        memset(out, 0, sizeof(out));

        #pragma omp parallel for
        for (size_t i=0; i<NPERMS; i++) {
            i64 initv[5];
            for (int j=0; j<5; j++)
                initv[j] = PERMS[i][j] + 5;
            out[i] = run_feedback_loop(initv);
        }

        for (size_t i=0; i<NPERMS; i++) {
            if (out[i] > part2) {
                part2 = out[i];
            }
        }
    }

    const clock_t t1 = clock();
    const double ttotal = 1000. * (t1 - t0) / CLOCKS_PER_SEC;
    printf("Part 2: done ... %.2fms\n", ttotal / NSAMPLE);

    printf("%" PRIi64 "\n", part1);
    printf("%" PRIi64 "\n", part2);
    return 0;
}
