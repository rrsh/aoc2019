#include "common.h"
#include "intcode.h"

static i64 program[VM_MEMSZ];
static struct vm_ctx vm;

#define N 256
static char grid[N][N];
static char vis[N][N];
static int minx = INT_MAX;
static int maxx = 0;
static int miny = INT_MAX;
static int maxy = 0;

static int
read_program(i64 v[], size_t *len, FILE *f)
{
    size_t i = 0;
    i64 x;

    do {
        if (fscanf(f, "%" SCNi64 , &x) != 1) {
            assert(!"bad input");
        }
        assert(i + 1 < VM_MEMSZ);
        v[i++] = x;
    } while (fscanf(f, " ,") != EOF);

    return *len = i + 1;
}

static int
turtle_run(i64 mem[], size_t n)
{
    char (*const g)[N] = (void *)&grid[N/2][N/2];
    char (*const v)[N] = (void *)&vis[N/2][N/2];
    int painted = 1;
    minx = INT_MAX;
    maxx = 0;
    miny = INT_MAX;
    maxy = 0;

    vm_init(&vm, mem, n, NULL, NULL);
    Vec2i pos = P(0, 0);
    int dir = 0;

    vis[N/2][N/2] = 1;
    vm_push_input(&vm, grid[N/2][N/2]);

    while (!vm_ishalted(&vm)) {
        vm_run(&vm);

        while (vm.outlen >= 2) {
            i64 rot, color;
            vm_pop_output(&vm, &rot);
            vm_pop_output(&vm, &color);

            if (pos.x < minx) { minx = pos.x;}
            if (pos.x > maxx) { maxx = pos.x;}
            if (pos.y < miny) { miny = pos.y;}
            if (pos.y > maxy) { maxy = pos.y;}

            g[pos.y][pos.x] = color;
            dir = (dir + (2*rot - 1)) & 3;
            pos = vec2i_add(pos, vec2i_fromdir(dir));

            if (vm_push_input(&vm, g[pos.y][pos.x])) {
                assert(!"input queue overflow");
            }

            if (!v[pos.y][pos.x]) {
                v[pos.y][pos.x] = 1;
                painted++;
            }
        }
    }

    return painted;
}

int main(int argc, char *argv[])
{
    if (argv[1]) {
        freopen(argv[1], "r", stdin);
    }

    size_t n;
    read_program(program, &n, stdin);

    int part1 = turtle_run(program, n);

    memset(grid, 0, sizeof(grid));
    memset(vis, 0, sizeof(vis));
    grid[N/2][N/2] = 1;

    turtle_run(program, n);

    printf("%d\n", part1);
    for (int y=maxy; y>=miny; y--) {
        for (int x=minx; x<maxx; x++) {
            int color = grid[N/2+y][N/2+x];
            fputc(color? '*' : ' ', stdout);
        }
        fputc('\n', stdout);
    }
    return 0;
}
